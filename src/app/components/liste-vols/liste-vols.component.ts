import { Component,Input,Output,EventEmitter } from '@angular/core';
import { Vol } from '../../models/vol.model';
import { VolComponent } from '../vol/vol.component';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-liste-vols',
  standalone: true,
  imports: [VolComponent,CommonModule],
  templateUrl: './liste-vols.component.html',
  styleUrls: ['./liste-vols.component.scss']
})
export class ListeVolsComponent {
  @Input() vols: Vol[] = [];
  @Output() selectVol = new EventEmitter<Vol>();
  @Input() type!: 'decollages' | 'atterrissages';

  onSelectVol(vol: Vol) {
    this.selectVol.emit(vol);
  }
}
