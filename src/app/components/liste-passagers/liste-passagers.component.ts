import { Component,Input } from '@angular/core';
import { Passager } from '../../models/passager.model';
import { MatSlideToggle } from "@angular/material/slide-toggle";
import { FormsModule } from '@angular/forms';
import { PassagerComponent } from '../passager/passager.component';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-liste-passagers',
  standalone: true,
  imports: [MatSlideToggle, PassagerComponent,FormsModule,CommonModule],
  templateUrl: './liste-passagers.component.html',
  styleUrls: ['./liste-passagers.component.scss']
})
export class ListePassagersComponent {
  @Input() passagers!: Passager[];

  isChecked: boolean = true;

}
