import { Component } from '@angular/core';
import { FiltresComponent } from '../filtres/filtres.component';
import { ListeVolsComponent } from '../liste-vols/liste-vols.component';
import { ListePassagersComponent } from '../liste-passagers/liste-passagers.component';
import { Vol } from '../../models/vol.model';
import { VolService } from '../../services/vol.service';
import { IAeroport } from '../../models/aeroport.model';
import { PassagerService } from '../../services/passager.service';
import { IPassager } from '../../models/passager.model';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-view-airfrance',
  standalone: true,
  imports: [FiltresComponent, ListeVolsComponent, ListePassagersComponent],
  templateUrl: './view-airfrance.component.html',
  styleUrls: ['./view-airfrance.component.scss']
})
export class ViewAirFranceComponent {
  vols: Vol[] = [];
  passagers: IPassager[] = [];
  type!: 'decollages' | 'atterrissages';

  constructor(private volService: VolService, private passagerService: PassagerService,private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(data => {
      this.type = data['type'];
    });
  }
  onSelectVol(vol: Vol) {
    this.passagerService.getPassagers(vol.icao).subscribe((passagers: IPassager[]) => {
      this.passagers = passagers;
    });
  }

  onFiltresChange(filtres: { aeroport: IAeroport, dateDebut: Date, dateFin: Date }) {
    const { aeroport, dateDebut, dateFin } = filtres;
    const debutSeconds = Math.floor(dateDebut.getTime() / 1000);
    const finSeconds = Math.floor(dateFin.getTime() / 1000);

    if (this.type === 'decollages') {
      this.volService.getVolsDepart(aeroport.icao, debutSeconds, finSeconds).subscribe((vols: Vol[]) => {
        this.vols = vols;
      });
    } else if (this.type === 'atterrissages') {
      this.volService.getVolsArrivee(aeroport.icao, debutSeconds, finSeconds).subscribe((vols: Vol[]) => {
        this.vols = vols;
      });
    }
}}
