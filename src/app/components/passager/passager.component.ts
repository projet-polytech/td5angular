import { Component, Input } from '@angular/core';
import { IPassager } from '../../models/passager.model';
import { CommonModule } from '@angular/common';
import { ClasseVolColorDirective } from '../../directives/classe-vol-color.directive';
import { BagageValideDirective } from '../../directives/bagage-valide.directive';

@Component({
  selector: 'app-passager',
  standalone: true,
  imports: [CommonModule, ClasseVolColorDirective, BagageValideDirective],
  templateUrl: './passager.component.html',
  styleUrls: ['./passager.component.scss']
})
export class PassagerComponent {
  @Input() passager!: IPassager;
  @Input() displayPhoto: boolean= true;

}
