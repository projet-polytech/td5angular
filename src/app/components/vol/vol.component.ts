import { Component,Input,Output, EventEmitter } from '@angular/core';
import { Vol } from '../../models/vol.model';
import { MatIconModule } from '@angular/material/icon';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-vol',
  standalone: true,
  imports: [MatIconModule, CommonModule],
  templateUrl: './vol.component.html',
  styleUrls: ['./vol.component.scss']
})
export class VolComponent {
  @Input() vol!: Vol;
  @Output() selectVol = new EventEmitter<Vol>();
  @Input() type!: 'decollages' | 'atterrissages';

  onSelectVol() {
    this.selectVol.emit(this.vol);
  }

  getLogo(compagnie: string): string {
    switch (compagnie) {
      case 'Air France':
        return 'assets/Air France.png';
      case 'Transavia France':
        return 'assets/Transavia France.png';
      case 'Air France Hop':
        return 'assets/Air France Hop.png';
      default:
        return '';
    }
  }
}
