import { Directive,ElementRef, Input, OnInit} from '@angular/core';

@Directive({
  selector: '[appBagageValide]',
  standalone: true
})
export class BagageValideDirective{
  @Input() classeVol!: string;
  @Input() appBagageValide!: number;

  constructor(private el: ElementRef) { }

  ngOnInit() {
    this.BagageLimit();
  }

  private BagageLimit() {
    switch (this.classeVol) {
      case 'BUSINESS':
        if (this.appBagageValide > 2) {
          this.el.nativeElement.style.backgroundColor = 'red';
        }
        break;
      case 'PREMIUM':
        if (this.appBagageValide > 3) {
          this.el.nativeElement.style.backgroundColor = 'red';
        }
        break;
      case 'STANDARD':
        if (this.appBagageValide> 1) {
          this.el.nativeElement.style.backgroundColor = 'red';
        }
        break;
    }
  }
}
