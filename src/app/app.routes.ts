import { Routes } from '@angular/router';
import { ViewAirFranceComponent } from './components/view-airfrance/view-airfrance.component';
import { RouterModule} from '@angular/router';
import { NgModule } from '@angular/core';

export const routes: Routes = [
  { path: 'decollages', component: ViewAirFranceComponent, data: { type: 'decollages' } },
  { path: 'atterrissages', component: ViewAirFranceComponent, data: { type: 'atterrissages' } },
  { path: '', redirectTo: '/decollages', pathMatch: 'full' },
  { path: '**', redirectTo: '/decollages' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
